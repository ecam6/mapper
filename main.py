import os
import pika
import json
import logging

import Broker

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter

QUEUE_MAPPER_MAIN = "/ptl/mapper/main"
QUEUE_MAPPED_FRONT = "mqtt-subscription-front.any.qos0"
QUEUE_MAPPED_GTW = "/ptl/mapped/gateway"

host = "http://" + os.getenv("API_HOST", "api") + ":" + os.getenv("API_PORT", "8000")
client = new_client(RequestsAdapter(), Configuration(host=host))


def find_box_in_workstation(in_workstation, article_id_ext, is_id_ext):
    # Get target article
    articles = client.articles.articles_list()
    art_filter = [
        a for a in articles if a.id_ext == article_id_ext or a.name == article_id_ext
    ]
    if len(art_filter) == 0:
        raise Exception(
            "No article found for article.id_ext <" + str(article_id_ext) + ">"
        )
    article = art_filter[0]
    # find article in workstation
    workstations = client.workstations.workstations_list()
    nb_bac = 0
    for w in workstations.results:
        if (is_id_ext == True and str(w.id_ext) == str(in_workstation)) or (is_id_ext == False and str(w.pos) == str(in_workstation)):
            for l in w.lines:
                for b in l.boxes:
                    nb_bac = nb_bac + 1
                    if b.article == article.id:
                        return w, l, b, article, nb_bac
    raise Exception("No workstation <"+str(in_workstation)+"> with article <"+str(article_id_ext)+"> found")


def get_settings():
    color = "#FFFFFF"
    brightness = 255
    for setting in client.settings.settings_list().results:
        if setting.key == "LED_COLOR":
            color = setting.value
        elif setting.key == "LED_BRIGHTNESS":
            brightness = int(setting.value)
    return color, brightness


def on_message(channel, method_frame, header_frame, body):
    try:
        in_message = json.loads(body)
        logging.info("[-] Receive MQTT message : queue <" + QUEUE_MAPPER_MAIN + "> data " + str(in_message))
        if 'id_workstation_ext' in in_message:
            in_workstation = in_message["id_workstation_ext"]
            is_id_ext = True 
        elif 'workstation' in in_message:
            in_workstation = in_message["workstation"]
            is_id_ext = False 
        else:
            raise Exception("Unable to decode the workstation")
        workstation, line, box, article, nb_bac = find_box_in_workstation(in_workstation, in_message["article"], is_id_ext)
        client.boxes.boxes_partial_update({"is_on": in_message["state"]}, box.id) # update box status
        out_message = None
        payload = json.dumps(
            {"state": in_message["state"], "box_id": box.id, "color": "purple"}
        )
        logging.info(
            "[x] Sending MQTT message : queue <"
            + QUEUE_MAPPED_FRONT
            + "> data "
            + str(payload)
        )
        channel.basic_publish(
            exchange="amq.topic", routing_key=QUEUE_MAPPED_FRONT, body=payload
        )  # send info to front
        color, brightness = get_settings()

        if workstation.iolink:  # If workstation is linked to a iolink
            iolink = client.iolinks.iolinks_retrieve(workstation.iolink)
            if workstation.iolink_port == None:
                raise Exception(
                    "Iolink master port is not set for the workstation <"
                    + str(workstation.pos)
                    + ">"
                )
            out_message = json.dumps(
                {
                    "controller": {
                        "id": workstation.iolink,
                        "type": "iolink",
                        "ip": iolink.ip,
                        "port": workstation.iolink_port,
                        "slave_port": nb_bac - 1,
                    },
                    "state": in_message["state"],
                    "color": color,
                    "brightness": brightness,
                }
            )
        else:  # assume it's for esp
            out_message = json.dumps(
                {
                    "nb_bac": nb_bac,
                    "workstation_pos": workstation.pos,
                    "state": in_message["state"],
                    "color": color,
                    "brightness": brightness,
                }
            )

        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        logging.info(
            "[x] Sending MQTT message : queue <"
            + QUEUE_MAPPED_GTW
            + "> data "
            + str(out_message)
            + " ESP"
        )
        channel.basic_publish(
            exchange="", routing_key=QUEUE_MAPPED_GTW, body=out_message
        )

    except Exception as e:
        logging.error(e)
        pass


if __name__ == "__main__":
    RABBIT_MQ_URL = os.getenv("RABBIT_MQ_URL", "broker")
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        level=logging.INFO,
        datefmt="[%d/%m/%Y %H:%M:%S]",
    )

    channel, connection = Broker.Setup(
        RABBIT_MQ_URL, [QUEUE_MAPPER_MAIN, QUEUE_MAPPED_FRONT, QUEUE_MAPPED_GTW]
    )

    channel.basic_consume(QUEUE_MAPPER_MAIN, on_message)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
        logging.info("[MAIN] STOP process due to keyboard interrupt")
    connection.close()
    logging.info("[MAIN] Bye")
