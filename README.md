# Mapper Pick to light

Ce projet appartient au projet [pick to light](https://gitlab.com/ecam6/ptl_apps) et se base sur [Python](https://www.python.org/).

Son rôle est avec un minimum d'informations reçu du provider ou du simulateur:
    - le poste
    - l'article
    - état
Retrouver et formater toutes les informations nécessaires :
    - couleur
    - luminosité
    - numéro de bac
    - informations controleur
    - ...
Pour les envoyer à la gateway.

# Documentation

`Broker.py` contient les éléments de connexion et reconnexion au message broker.

`main.py` est le point d'entrée du programme, connexion, on_message...

# Variable d'environement

- API_HOST (default : api)
- API_PORT (default: 8000)
- RABBIT_MQ_URL (default: broker)

# Déploiement, usage, configuration...

Ne peut pas être utilisé seul, se référé au [projet principal](https://gitlab.com/ecam6/ptl_apps)
