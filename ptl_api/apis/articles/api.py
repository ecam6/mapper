from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import articles_list
from . import articles_create
from . import articles_retrieve
from . import articles_update
from . import articles_partial_update
from . import articles_destroy
from . import articles_sync_retrieve
class ArticlesApi(BaseApi):
    articles_list = articles_list.make_request
    articles_create = articles_create.make_request
    articles_retrieve = articles_retrieve.make_request
    articles_update = articles_update.make_request
    articles_partial_update = articles_partial_update.make_request
    articles_destroy = articles_destroy.make_request
    articles_sync_retrieve = articles_sync_retrieve.make_request