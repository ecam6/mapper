from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class Box(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    is_on: typing.Optional[bool]  = None
    line: str 
    pos: int 
    size: str 

def make_request(self: BaseApi,

    __request__: Box,


    id: str,

) -> Box:
    

    
    body = __request__
    

    m = ApiRequest(
        method="PUT",
        path="/boxes/{id}/".format(
            
                id=id,
            
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": Box,
            
        },
    
    }, m)