from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import boxes_list
from . import boxes_create
from . import boxes_retrieve
from . import boxes_update
from . import boxes_partial_update
from . import boxes_destroy
class BoxesApi(BaseApi):
    boxes_list = boxes_list.make_request
    boxes_create = boxes_create.make_request
    boxes_retrieve = boxes_retrieve.make_request
    boxes_update = boxes_update.make_request
    boxes_partial_update = boxes_partial_update.make_request
    boxes_destroy = boxes_destroy.make_request